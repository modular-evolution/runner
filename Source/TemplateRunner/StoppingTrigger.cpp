// Fill out your copyright notice in the Description page of Project Settings.

#include "StoppingTrigger.h"
#include "FloatVariable.h"
#include "TimerManager.h"
#include "Engine/Public/TimerManager.h"


void AStoppingTrigger::OnInternalPlayerEnter(class ATemplateRunnerPawn* playeyr)
{
	PreviousMovementSpeed = UFloatVariable::GetFloatValue(MovementSpeedReference);
	UFloatVariable::SetFloatValue(MovementSpeedReference, 0.f);

	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &AStoppingTrigger::StartMovementAgain, stoppingDuration, false);
}

void AStoppingTrigger::StartMovementAgain()
{
	UFloatVariable::SetFloatValue(MovementSpeedReference, PreviousMovementSpeed);
}
