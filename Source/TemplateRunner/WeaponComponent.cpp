// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponComponent.h"
#include "Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "StringVariable.h"
#include "BoolVariable.h"
#include "CoreMinimal.h"

//TODO: Break this down.
bool UWeaponComponent::Fire()
{
	if (UBoolVariable::GetBoolValue(IsReloading) == true)
	{
		return false;
	}

	// Check if fire rate passed.
	float currentTime = GetWorld()->GetRealTimeSeconds();
	if (currentTime < NextFireTime)
	{
		return false;
	}

	// Calculate the new fire rate.
	NextFireTime = currentTime + FireRate;

	// Find the muzzle transform.
	FTransform spawnTransform = GetSocketTransform("Muzzle");

	// Line trace.
	FHitResult RV_Hit(ForceInit);
	if (DoTrace(&RV_Hit))
	{
		CurrentMagazine--;
		if (CurrentMagazine <= 0)
		{
			StartReloading();
		}

		// Calculate the spawn details.
		FVector spawnLocation = spawnTransform.GetLocation();
		FRotator spawnRotation = (RV_Hit.Location - spawnLocation).Rotation();

		// Spawn the actor in the world.
		GetWorld()->SpawnActor<AProjectile>(ProjectileClass, spawnLocation, spawnRotation);

		// Update the text on the UI.
		UpdateAmmoText();

		// We successfully shot a bullet.
		return true;
	}

	return false;
}

bool UWeaponComponent::DoTrace(FHitResult* RV_Hit)
{
	// get the camera transform
	auto controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	// Calculate the screen middle point.
	int ScreenX, ScreenY;
	controller->GetViewportSize(ScreenX, ScreenY);

	// Trace from the middle of the screen.
	bool DidTrace = controller->GetHitResultAtScreenPosition(FVector2D(ScreenX/2, ScreenY/2), ECollisionChannel::ECC_MAX, true, *RV_Hit);

	return DidTrace;
}

void UWeaponComponent::BeginPlay()
{
	FinishReloading();
}

void UWeaponComponent::Reload()
{
	if (!CheckMagazinFull())
	{
		StartReloading();
	}
}

void UWeaponComponent::StartReloading()
{
	UBoolVariable::SetBoolValue(IsReloading, true);
	
	GetOwner()->GetWorldTimerManager().SetTimer(reloadHandle, this, &UWeaponComponent::FinishReloading, ReloadingTime);
}

void UWeaponComponent::FinishReloading()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(reloadHandle);
	UBoolVariable::SetBoolValue(IsReloading, false);

	CurrentMagazine = MagazineSize;
	UpdateAmmoText();
}

void UWeaponComponent::UpdateAmmoText()
{
	FString currentAmmoText = FString::FromInt(CurrentMagazine) + "/" + FString::FromInt(MagazineSize);
	AmmoText->SetStringValue(AmmoText, currentAmmoText);
}

