// Fill out your copyright notice in the Description page of Project Settings.

#include "MobileInputComponent.h"

void UMobileInputComponent::OnTouchDown(ETouchIndex::Type Type, FVector location)
{
	bIsTouchDown = true;
	touchBegin = location;
}

void UMobileInputComponent::OnTouchUp(ETouchIndex::Type Type, FVector location)
{
	CalculateInput(location);

	bIsTouchDown = false;
	touchBegin = FVector::ZeroVector;
}

void UMobileInputComponent::OnTouchMoved(ETouchIndex::Type Type, FVector location)
{
	FVector swipeDirection = location - touchBegin;
	if (swipeDirection.Size() > swipeMaxDistance)
	{
		OnTouchUp(Type, location);
	}
}

void UMobileInputComponent::CalculateInput(FVector touchEnd)
{
	if (bIsTouchDown == false)
	{
		return;
	}

	FVector swipeDirection = touchEnd - touchBegin;

	float	swipeLength;
	FVector swipeUnit;
	swipeDirection.ToDirectionAndLength(swipeUnit, swipeLength);

	if (swipeLength < swipeMinDistance)
	{
		SendTouchEvent();
	}
	else
	{
		float XMovement = FMath::Abs(swipeUnit.X);
		float YMovement = FMath::Abs(swipeUnit.Y);

		if (XMovement > YMovement)
		{
			if (swipeDirection.X > 0)
			{
				SendSwipeEvent(ESwipeDirection::SD_Right);
			}
			else if (swipeDirection.X < 0)
			{
				SendSwipeEvent(ESwipeDirection::SD_Left);
			}
		}
		else
		{
			if (swipeDirection.Y < 0)
			{
				SendSwipeEvent(ESwipeDirection::SD_Up);
			}
			else if (swipeDirection.Y > 0)
			{
				SendSwipeEvent(ESwipeDirection::SD_Down);
			}
		}
	}
}

void UMobileInputComponent::SendSwipeEvent(ESwipeDirection direction)
{
	switch (direction)
	{
	case ESwipeDirection::SD_Up:
		SwipeUp.Broadcast();
		break;
	case ESwipeDirection::SD_Down:
		SwipeDown.Broadcast();
		break;
	case ESwipeDirection::SD_Right:
		SwipeRight.Broadcast();
		break;
	case ESwipeDirection::SD_Left:
		SwipeLeft.Broadcast();
		break;
	}
}

void UMobileInputComponent::SendTouchEvent()
{
	TouchDown.Broadcast();
}
