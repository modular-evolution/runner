// Fill out your copyright notice in the Description page of Project Settings.

#include "TargetableActor.h"
#include "TemplateRunnerGameState.h"
#include "Engine/World.h"
#include "Components/SceneComponent.h"

// Sets default values for this component's properties
ATargetableActor::ATargetableActor()
{
	maxHealth = 5;
	currentHealth = maxHealth;
}

// Deals damage to the target.
void ATargetableActor::DamageTarget(float damage)
{
	// Calculate new health.
	currentHealth -= damage;

	// Check the current health.
	if (currentHealth <= 0)
	{
		OnKilled();
	}
	else
	{
		OnDamageTaken();
	}
}

FVector ATargetableActor::GetTargetAimLocation()
{
	return GetActorLocation() + TargetingOffset;
}

// Reset the target's health to full health.
void ATargetableActor::ResetHealth()
{
	// Reset the current health to the max health.
	currentHealth = maxHealth;
}

// Registers the current target to the possible ones in game state.
void ATargetableActor::RegisterSelf()
{
	// Add the target to the possible targets inside the game state.
	auto GameState = GetWorld()->GetGameState<ATemplateRunnerGameState>();
	if (GameState)
	{
		GameState->AddTarget(this);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Race condition lost."))
	}
}



