// Fill out your copyright notice in the Description page of Project Settings.

#include "SplinePath.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "FRotatorVariable.h"
#include "FVectorVariable.h"
#include "FloatVariable.h"

// Sets default values
ASplinePath::ASplinePath()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a spline to define the curve.
	SplinePath = CreateDefaultSubobject<USplineComponent>("Spline Path");

	// Set as root component.
	SetRootComponent(SplinePath);
}

// Constructs the spline's mesh.
void ASplinePath::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (this->IsPendingKill())
	{
		return;
	}

	if (SplineMesh == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Mesh for the spline's mesh component on %s"), *GetName())
			return;
	}

	// Clear the children to free memory.
	ClearChildrenFromSpline();

	for (int i = 0; i < SplinePath->GetNumberOfSplinePoints() - 1; i++)
	{
		// Calculate starting details.
		FVector StartLocation;
		FVector StartTangent;

		SplinePath->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);

		// Calculate ending details.
		FVector EndLocation;
		FVector EndTangent;

		SplinePath->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);

		// Create the mesh for the spline and register component.
		USplineMeshComponent* s = NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass());
		s->RegisterComponent();

		// Set the mobility and physics..
		s->SetMobility(EComponentMobility::Movable);

		// Set physics.
		if (bEnablePhysics)
		{
			s->SetCollisionResponseToAllChannels(ECR_Block);
			s->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}
		
		// Add the mesh and deform it.
		s->SetStaticMesh(SplineMesh);
		s->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent, true);

		// Attach deformed mesh to the spline (root).
		s->AttachToComponent(SplinePath, FAttachmentTransformRules::KeepRelativeTransform);
	}
}


// Cleas the current children.
void ASplinePath::ClearChildrenFromSpline()
{
	// Get all current children
	auto children = SplinePath->GetAttachChildren();

	// Remove the current Children to clear memory.
	for (int i = 0; i < children.Num(); i++)
	{
		if (IsValid(children[i]))
		{
			children[i]->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
			children[i]->DestroyComponent();
		}
	}
}


void ASplinePath::UpdateSplinePosition(float DeltaTime)
{
	float DeltaMovement = DeltaTime * UFloatVariable::GetFloatValue(MovementSpeed);
	float NewDistance = UFloatVariable::GetFloatValue(SplineDistance) + DeltaMovement;
	UFloatVariable::SetFloatValue(SplineDistance, NewDistance);

	FVector NewVector = GetSplineLocation(NewDistance);
	UFVectorVariable::SetFVectorValue(SplineLocation, NewVector);

	FRotator NewRotator = GetSplineRotation(NewDistance);
	UFRotatorVariable::SetFRotatorValue(SplineRotation, NewRotator);
}

FVector ASplinePath::GetSplineLocation(float distance)
{
	// Find the transform at the point.
	return SplinePath->GetLocationAtDistanceAlongSpline(distance, ESplineCoordinateSpace::World);
}

FRotator ASplinePath::GetSplineRotation(float distance)
{
	return SplinePath->GetRotationAtDistanceAlongSpline(distance, ESplineCoordinateSpace::World);
}

