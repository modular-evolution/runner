// Fill out your copyright notice in the Description page of Project Settings.

#include "TemplateRunnerPawn.h"
#include "Components/CapsuleComponent.h"
#include "Engine/CollisionProfile.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Camera/CameraShake.h"
#include "Kismet/GameplayStatics.h"
#include "FVectorVariable.h"
#include "FRotatorVariable.h"
#include "GameFramework/Actor.h"
#include "TemplateRunnerGameState.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "WeaponComponent.h"
#include "TargetableActor.h"
#include "ShopData.h"
#include "MobileInputComponent.h"
#include "ShopWeapon.h"

// Sets default values
ATemplateRunnerPawn::ATemplateRunnerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(FName("CapsuleCollider"));
	CapsuleComponent->InitCapsuleSize(34.0f, 88.0f);
	CapsuleComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);

	CapsuleComponent->CanCharacterStepUpOn = ECB_No;
	CapsuleComponent->SetShouldUpdatePhysicsVolume(true);
	CapsuleComponent->bCheckAsyncSceneOnMove = false;
	CapsuleComponent->SetCanEverAffectNavigation(false);
	CapsuleComponent->bDynamicObstacle = true;
	RootComponent = CapsuleComponent;

	Mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(FName("MeshComponent"));
	Mesh->AlwaysLoadOnClient = true;
	Mesh->AlwaysLoadOnServer = true;
	Mesh->bOwnerNoSee = false;
	Mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPose;
	Mesh->bCastDynamicShadow = true;
	Mesh->bAffectDynamicIndirectLighting = true;
	Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	Mesh->SetupAttachment(CapsuleComponent);
	static FName MeshCollisionProfileName(TEXT("CharacterMesh"));
	Mesh->SetCollisionProfileName(MeshCollisionProfileName);
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCanEverAffectNavigation(false);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	initialRotation = FollowCamera->GetComponentRotation();

	// Create the mobile input component to create mobile specific input.
	MobileInputComponent = CreateDefaultSubobject<UMobileInputComponent>(TEXT("MobileInputComponent"));
}

void ATemplateRunnerPawn::BeginPlay()
{
	Super::BeginPlay();
	//TODO: Create component at runtime.
	auto currentWeapon = UShopData::GetWeaponAtIndex(shopData, weaponIndex);
	
	FAttachmentTransformRules rules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, true);
	Weapon = NewObject<UWeaponComponent>(this, currentWeapon->WeaponBP, FName("Weapon"));
	Weapon->RegisterComponent();
	Weapon->AttachToComponent(Mesh, rules, FName("GripPoint"));
}

void ATemplateRunnerPawn::CallScreenShake(TSubclassOf<UCameraShake> cameraShake)
{
	UGameplayStatics::PlayWorldCameraShake(
		GetWorld(),
		cameraShake,
		GetActorLocation(),
		0.f,
		500.f
		);
}

void ATemplateRunnerPawn::SetPosition(UFVectorVariable* NewPosition)
{
	FVector NewWorldPosition = UFVectorVariable::GetFVectorValue(NewPosition);
	NewWorldPosition.Z = GetActorLocation().Z;

	SetActorLocation(NewWorldPosition);
}

void ATemplateRunnerPawn::AimAtCurrentTarget(UFRotatorVariable* NewRotation)
{
	FRotator TargetRotation;
	FRotator CameraRotation;

	if (IsValid(CurrentTarget))
	{
		auto TargetPosition = CurrentTarget->GetTargetAimLocation();
		TargetRotation = (TargetPosition - GetActorLocation()).Rotation();
		CameraRotation = (TargetPosition - FollowCamera->GetComponentLocation()).Rotation();
	}
	else
	{
		TargetRotation = UFRotatorVariable::GetFRotatorValue(NewRotation);
		CameraRotation = FollowCamera->GetComponentTransform().TransformRotation(initialRotation.Quaternion()).Rotator();
		GetNewTarget();
	}

	FRotator NewActorRotation = FMath::Lerp(GetActorRotation(), TargetRotation, rotationSpeed);
	SetActorRotation(NewActorRotation);
	FRotator NewCameraRotation = FMath::Lerp(FollowCamera->GetComponentRotation(), CameraRotation, rotationSpeed);
	FollowCamera->SetWorldRotation(NewCameraRotation);
}

void ATemplateRunnerPawn::GetNewTarget()
{
	auto GameState = GetWorld()->GetGameState<ATemplateRunnerGameState>();
	CurrentTarget = GameState->PopTarget();

	if (CurrentTarget != nullptr)
	{
		OnTargetFound();
	}
}

void ATemplateRunnerPawn::FireWeapon()
{
	bool didFire = Weapon->Fire();
	if (didFire)
	{
		OnShotFired();
	}
}

void ATemplateRunnerPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	// Delegate touch input.
	PlayerInputComponent->BindTouch(IE_Pressed, MobileInputComponent, &UMobileInputComponent::OnTouchDown);
	PlayerInputComponent->BindTouch(IE_Released, MobileInputComponent, &UMobileInputComponent::OnTouchUp);
	PlayerInputComponent->BindTouch(IE_Repeat, MobileInputComponent, &UMobileInputComponent::OnTouchMoved);
}

