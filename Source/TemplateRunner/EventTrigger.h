// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EventTrigger.generated.h"

UCLASS()
class TEMPLATERUNNER_API AEventTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEventTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the player enteres the trigger.
	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerEnter(class ATemplateRunnerPawn* player);

	virtual void OnInternalPlayerEnter(ATemplateRunnerPawn* playeyr);

	UFUNCTION()
	void OnActorBeginOverlapSolved(AActor* overlappedActor, AActor* otherActor);
	
	// Box Collier used for collision detection.
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxComponent;
};
