// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Object.h"
#include "UObject/ObjectMacros.h"
#include "BaseVariable.h"
#include "ShopData.generated.h"

/**
 * 
 */

class UShopWeapon;

UCLASS(BlueprintType)
class TEMPLATERUNNER_API UShopData : public UBaseVariable
{
	GENERATED_BODY()
	
public:
 	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shop System")
 		TArray<UShopWeapon*> Weapons;
 
 	UFUNCTION(BlueprintPure, Category = "Shop")
 		static UShopWeapon* GetWeaponAtIndex(UShopData* var, int32 index);
 		
	UFUNCTION(BlueprintPure, Category = "Shop")
		static TArray<UShopWeapon*> GetAllWeapons(UShopData* var);
};
