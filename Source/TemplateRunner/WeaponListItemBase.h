// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ShopData.h"
#include "WeaponListItemBase.generated.h"

/**
 * 
 */
UCLASS()
class TEMPLATERUNNER_API UWeaponListItemBase : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
		void InitializeWeaponItem(UShopWeapon* data);

	UFUNCTION(BlueprintImplementableEvent)
		void OnInitializeWeaponItem(UShopWeapon* data);

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UShopWeapon* weaponData;
		
};
