// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "WeaponComponent.generated.h"

/**
 * 
 */

class UStringVariable;
class UBoolVariable;

UCLASS(Blueprintable)
class TEMPLATERUNNER_API UWeaponComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()

public:
	// Shots a bullet.
	bool Fire();
	bool DoTrace(FHitResult* RV_Hit);
	
protected:
	// How fast should the weapon fire?
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Firing)
	float FireRate;

	// Next time when the player is allowed to fire.
	float NextFireTime;

	// How many bullets should be inside the Magazin.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Firing)
	int32 MagazineSize;

	// How many bullets are left inside the current magazin.
	int32 CurrentMagazine;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Firing)
	float ReloadingTime;

	// Projectile spawned by the weapon.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = References)
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = References)
	UStringVariable* AmmoText;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = References)
	UBoolVariable* IsReloading;

	// Reset ammo count on the start.
	virtual void BeginPlay();

public:
	UFUNCTION(BlueprintCallable)
		void Reload();


private:
	FTimerHandle reloadHandle;

	void StartReloading();

	void FinishReloading();

	void UpdateAmmoText();

	bool CheckMagazinFull() { return CurrentMagazine >= MagazineSize; }
};
