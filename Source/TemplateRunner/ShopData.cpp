// Fill out your copyright notice in the Description page of Project Settings.

#include "ShopData.h"
#include "ShopWeapon.h"



UShopWeapon* UShopData::GetWeaponAtIndex(UShopData* var, int32 index)
{
	if (index >= 0 && index < var->Weapons.Num())
	{
		return var->Weapons[index];
	}
	else
	{
		return nullptr;
	}
}

TArray<UShopWeapon*> UShopData::GetAllWeapons(UShopData* var)
{
	return var->Weapons;
}
