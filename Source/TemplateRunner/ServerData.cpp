// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerData.h"


// Sets default values
UServerData::UServerData(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//When the object is constructed, Get the HTTP module
	Http = &FHttpModule::Get();

	UpdateVariablesFromServer();
}

/*Http call*/
void UServerData::UpdateVariablesFromServer()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &UServerData::OnResponseReceived);
	//This is the url on which to process the request
	Request->SetURL("http://alexandruoprea.fragmentedpixel.com/Modular-Evolution/getint.php");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

/*Assigned function on successfull http call*/
void UServerData::OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	UE_LOG(LogTemp, Warning, TEXT("Data recieved: %s"), *Response->GetContentAsString())

	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		IsSaleOn = JsonObject->GetBoolField("saleOn");
		DiscountPercent = JsonObject->GetNumberField("discountPercent");

		OnDataRecieved();
	}
}