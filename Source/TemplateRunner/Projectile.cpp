// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TargetableActor.h"

AProjectile::AProjectile()
{
	// Create the mesh component.
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::SolveOverlap);
	SetRootComponent(MeshComponent);

	// Create the projectile movement component.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
}

void AProjectile::SolveOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	// Check if the actor is targetable.
	auto Target = Cast<ATargetableActor>(OtherActor);

	// Damage him if so.
	if (Target != nullptr)
	{
		Target->DamageTarget(damage);
	}

	// Impact callback.
	OnImpact(GetActorLocation(), GetActorRotation());

	// Destroy actor on collision.
	Destroy();
}
