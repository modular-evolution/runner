// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TargetableActor.generated.h"

class USceneComponent;

UCLASS()
class TEMPLATERUNNER_API ATargetableActor : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	ATargetableActor();

	// Deals damage to the target.
	UFUNCTION(BlueprintCallable, Category = Health)
		void DamageTarget(float damage);

	UFUNCTION(BlueprintCallable, Category = Targeting)
		FVector GetTargetAimLocation();

protected:
	// Max Health.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Health)
		float maxHealth;

	// Current Health.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Health)
		float currentHealth;

protected:
	// Reset the target's health to full health.
	UFUNCTION(BlueprintCallable, Category = Health)
		void ResetHealth();

	// Registers the current target to the possible ones in game state.
	UFUNCTION(BlueprintCallable, Category = Health)
		void RegisterSelf();

public:
	// Called when the target is damaged.
	UFUNCTION(BlueprintImplementableEvent)
		void OnDamageTaken();

	// Calaled when the target is killed.
	UFUNCTION(BlueprintImplementableEvent)
		void OnKilled();

protected:

	UPROPERTY(EditAnywhere, Category = Targeting)
		FVector TargetingOffset;
};
