// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TemplateRunner : ModuleRules
{
	public TemplateRunner(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "VarSystem", "ObsEvent", "Http", "Json", "JsonUtilities", "GameAnalytics", "OnlineSubsystem" } );
        PublicIncludePathModuleNames.AddRange( new string[] { "VarSystem", "ObsEvent", "Http", "Json", "JsonUtilities", "GameAnalytics" } );
        DynamicallyLoadedModuleNames.Add( "OnlineSubsystemGooglePlay" );
    }
}
