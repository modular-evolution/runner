// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EventTrigger.h"
#include "StoppingTrigger.generated.h"

/**
 * 
 */
UCLASS()
class TEMPLATERUNNER_API AStoppingTrigger : public AEventTrigger
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Trigger)
	class UFloatVariable* MovementSpeedReference;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trigger)
	float stoppingDuration;

	virtual void OnInternalPlayerEnter(class ATemplateRunnerPawn* playeyr) override;
	
	float PreviousMovementSpeed;

	void StartMovementAgain();
	
};
