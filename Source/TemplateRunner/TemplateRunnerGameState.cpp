// Fill out your copyright notice in the Description page of Project Settings.

#include "TemplateRunnerGameState.h"
#include "TargetableActor.h"

void ATemplateRunnerGameState::AddTarget(ATargetableActor* NewTarget)
{
	if (IsValid(NewTarget))
	{
		Targets.Add(NewTarget);
	}
}

ATargetableActor* ATemplateRunnerGameState::PopTarget()
{
	// There are no active targets avaiable.
	if (Targets.Num() <= 0)
	{
		return nullptr;
	}
	// Check if current target is valid.
	else if (IsValid(Targets[0]))
	{
		return Targets[0];
	}
	// Get next target.
	else
	{
		Targets.RemoveAt(0);
		return PopTarget();
	}
}

