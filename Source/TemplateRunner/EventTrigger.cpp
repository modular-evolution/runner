// Fill out your copyright notice in the Description page of Project Settings.

#include "EventTrigger.h"
#include "TemplateRunnerPawn.h"
#include "Components/BoxComponent.h"
#include "Engine/CollisionProfile.h"

// Sets default values
AEventTrigger::AEventTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(FName("Collider"));
	BoxComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	
	BoxComponent->CanCharacterStepUpOn = ECB_No;
	BoxComponent->SetShouldUpdatePhysicsVolume(true);
	RootComponent = BoxComponent;
}

// Called when the game starts or when spawned
void AEventTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorBeginOverlap.AddDynamic(this, &AEventTrigger::OnActorBeginOverlapSolved);
}


void AEventTrigger::OnInternalPlayerEnter(ATemplateRunnerPawn* playeyr)
{

}

void AEventTrigger::OnActorBeginOverlapSolved(AActor* overlappedActor, AActor* otherActor)
{
	ATemplateRunnerPawn* runner = Cast<ATemplateRunnerPawn>(otherActor);

	if (runner != nullptr)
	{
		OnInternalPlayerEnter(runner);
		OnPlayerEnter(runner);
	}
}
