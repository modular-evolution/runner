// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "ServerData.generated.h"

/**
 * 
 */
UCLASS()
class TEMPLATERUNNER_API UServerData : public UGameInstance
{
	GENERATED_BODY()
	
	
public:
	FHttpModule* Http;

	// Sets default values for this actor's properties
	UServerData(const class FObjectInitializer& ObjectInitializer);

	/* The actual HTTP call */
	UFUNCTION(BlueprintCallable)
	void UpdateVariablesFromServer();

	/*Assign this function to call when the GET request processes sucessfully*/
	void OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	UFUNCTION(BlueprintImplementableEvent)
	void OnDataRecieved();

	UPROPERTY(BlueprintReadWrite)
	bool IsSaleOn;

	UPROPERTY(BlueprintReadWrite)
	float DiscountPercent;
};


