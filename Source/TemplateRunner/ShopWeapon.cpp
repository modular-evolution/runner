// Fill out your copyright notice in the Description page of Project Settings.

#include "ShopWeapon.h"
#include "Kismet/GameplayStatics.h"

void UShopWeapon::Buy(UShopWeapon* var)
{
	var->Unlocked = true;
}

void UShopWeapon::BuyWeapon()
{
	Unlocked = true;
}

void UShopWeapon::Save()
{
	UGameplayStatics::SaveGameToSlot(this, SaveName.ToString(), 0);
}

void UShopWeapon::Load()
{
	UShopWeapon* LoadGameInstance = Cast<UShopWeapon>(UGameplayStatics::CreateSaveGameObject(UShopWeapon::StaticClass()));
	LoadGameInstance = Cast<UShopWeapon>(UGameplayStatics::LoadGameFromSlot(SaveName.ToString(), 0));

	if (LoadGameInstance != nullptr)
	{
		this->Unlocked = LoadGameInstance->Unlocked;
	}
}
