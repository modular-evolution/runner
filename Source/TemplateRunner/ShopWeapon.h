// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseVariable.h"
#include "ShopWeapon.generated.h"

/**
 * 
 */

class UWeaponComponent;

UCLASS(BlueprintType)
class TEMPLATERUNNER_API UShopWeapon : public UBaseVariable
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shop System")
		TSubclassOf<UWeaponComponent> WeaponBP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shop System")
		float Price;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shop System")
		bool Unlocked;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shop System")
		UTexture2D* PreviewImage;

	UFUNCTION(BlueprintCallable, Category = "Shop System")
		static void Buy(UShopWeapon* var);

	UFUNCTION(BlueprintCallable, Category = "Shop System")
		void BuyWeapon();

	/**** Base Variable Overrides ****/

	virtual void Save() override;

	virtual void Load() override;

	
	
};
