// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TemplateRunnerPawn.generated.h"

UCLASS()
class TEMPLATERUNNER_API ATemplateRunnerPawn : public APawn
{
	GENERATED_BODY()

protected:

	/** The current shop the player is using. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	int32 weaponIndex;

	/** The current shop the player is using. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	class UShopData* shopData;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** The main skeletal mesh associated with this Character (optional sub-object). */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh;

	/** The weapon skeletal mesh. Used to hold the gun information.*/
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWeaponComponent* Weapon;

	/** The CapsuleComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* CapsuleComponent;

	/** Transforms touches in mobile input. */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMobileInputComponent* MobileInputComponent;

public:
	// Sets default values for this pawn's properties
	ATemplateRunnerPawn();

protected:
	// Rotation speed of the character.
	UPROPERTY(Category = Character, EditAnywhere, BlueprintReadWrite)
	float rotationSpeed = .1f;

	// Target of the actor.
	class ATargetableActor* CurrentTarget;

	void BeginPlay() override;

	// Calls a screen shake with default parameters.
	UFUNCTION(BlueprintCallable)
	void CallScreenShake(TSubclassOf<UCameraShake> cameraShake);

	UFUNCTION(BlueprintCallable)
	// Sets the actor XY Position and copies the current Z one.
	void SetPosition(class UFVectorVariable* NewPosition);

	UFUNCTION(BlueprintCallable)
	void AimAtCurrentTarget(class UFRotatorVariable* NewRotation);

	UFUNCTION(BlueprintImplementableEvent)
	void OnShotFired();

	UFUNCTION(BlueprintImplementableEvent)
	void OnTargetFound();

	void GetNewTarget();

	UFUNCTION(BlueprintCallable)
	void FireWeapon();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
private:
	FRotator initialRotation;
};
