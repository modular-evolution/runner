// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EventTrigger.h"
#include "EnemySpawnerTrigger.generated.h"

/**
 * 
 */
UCLASS()
class TEMPLATERUNNER_API AEnemySpawnerTrigger : public AEventTrigger
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trigger)
	TSubclassOf<class ATargetableActor> EnemyToSpawn;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = Trigger)
	TArray<class ATargetPoint*> SpawnPoints;

	// Called internally when the player enters the trigger.
	virtual void OnInternalPlayerEnter(class ATemplateRunnerPawn* playeyr) override;
};
