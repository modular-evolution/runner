// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class TEMPLATERUNNER_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default properties for the actor.
	AProjectile();
	
protected:
	// How much damage does the bullet do.
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Shooting)
	float damage;

	// Projectile movement component that moves the actor.
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Shooting)
	class UProjectileMovementComponent* ProjectileMovementComponent;
	
	// Static mesh of the actor.
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Shooting)
	class UStaticMeshComponent* MeshComponent;

	// Solves the overlap of the mesh component.
	UFUNCTION()
		virtual void SolveOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult &SweepResult);

	// Called on imapct with the last location and rotation.
	UFUNCTION(BlueprintImplementableEvent)
		void OnImpact(FVector Location, FRotator Rotation);
};
