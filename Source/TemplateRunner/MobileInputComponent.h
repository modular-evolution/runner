// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MobileInputComponent.generated.h"

/**
 * 
 */

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESwipeDirection : uint8
{
	SD_Up		UMETA(DisplayName = "Up"),
	SD_Down		UMETA(DisplayName = "Down"),
	SD_Right	UMETA(DisplayName = "Right"),
	SD_Left		UMETA(DisplayName = "Left"),

	SD_COUNT
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMobileInput);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TEMPLATERUNNER_API UMobileInputComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:
	/************************************************************************/
	/* Flow Events															*/
	/************************************************************************/

	 // Flow of action when the event is called.
	UPROPERTY(BlueprintAssignable, Category = "ObsEvents")
		FOnMobileInput TouchDown;

	// Flow of action when the event is called.
	UPROPERTY(BlueprintAssignable, Category = "ObsEvents")
		FOnMobileInput SwipeRight;

	// Flow of action when the event is called.
	UPROPERTY(BlueprintAssignable, Category = "ObsEvents")
		FOnMobileInput SwipeLeft;

	// Flow of action when the event is called.
	UPROPERTY(BlueprintAssignable, Category = "ObsEvents")
		FOnMobileInput SwipeDown;

	// Flow of action when the event is called.
	UPROPERTY(BlueprintAssignable, Category = "ObsEvents")
		FOnMobileInput SwipeUp;

public:
	/************************************************************************/
	/* Input Delegators														*/
	/************************************************************************/
	UFUNCTION()
		void OnTouchDown(ETouchIndex::Type Type, FVector location);

	UFUNCTION()
		void OnTouchUp(ETouchIndex::Type Type, FVector location); 

	UFUNCTION()
		void OnTouchMoved(ETouchIndex::Type Type, FVector location);

	/************************************************************************/
	/* Debugging and forcing												*/
	/************************************************************************/

	UFUNCTION(BlueprintCallable)
	void SendSwipeEvent(ESwipeDirection direction);

	UFUNCTION(BlueprintCallable)
	void SendTouchEvent();

private:
	// Calculates a swipe or touch based on an end.
	void CalculateInput(FVector touchEnd);

protected:
	/************************************************************************/
	/* Settings																*/
	/************************************************************************/

	// Minimum movement to consider this a swipe.
	UPROPERTY(EditDefaultsOnly, Category = "Input", meta = (AllowPrivateAccess = true))
	float swipeMinDistance;

	// Maximum movement before considering this a swipe.
	UPROPERTY(EditDefaultsOnly, Category = "Input", meta = (AllowPrivateAccess = true))
		float swipeMaxDistance;

private:
	/************************************************************************/
	/* Internal Use															*/
	/************************************************************************/

	bool bIsTouchDown;

	float touchDuration;

	FVector touchBegin;

};
