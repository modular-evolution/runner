// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemySpawnerTrigger.h"
#include "Engine/TargetPoint.h"
#include "TargetableActor.h"
#include "Engine/World.h"


void AEnemySpawnerTrigger::OnInternalPlayerEnter(class ATemplateRunnerPawn* playeyr)
{
	auto world = GetWorld();
	for (int32 i = 0; i < SpawnPoints.Num(); i++)
	{
		ATargetPoint* currentPoint = SpawnPoints[i];
		world->SpawnActor<ATargetableActor>(EnemyToSpawn, currentPoint->GetActorLocation(), GetActorRotation());
	}
}
