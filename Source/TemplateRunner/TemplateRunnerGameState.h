// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TemplateRunnerGameState.generated.h"

/**
 * 
 */

class ATargetableActor;

UCLASS()
class TEMPLATERUNNER_API ATemplateRunnerGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	// Posibile targets to hit.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Enemies)
	TArray<ATargetableActor*> Targets;
		
	// Adds a new target to the current ones.
	UFUNCTION(BlueprintCallable, Category = Enemies)
	void AddTarget(ATargetableActor* NewTarget);

	// Gets the first target from the list.
	UFUNCTION(BlueprintCallable, Category = Enemies)
	ATargetableActor* PopTarget();


};
